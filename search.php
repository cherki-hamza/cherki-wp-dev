<?php get_header(); ?>


    <!-- start Main content-->

    <a href="">
        <h2 class="page-heading">this is the search result <?php echo get_search_query();  ?></h2>
    </a>

 <?php if (have_posts()){  ?>
    <!-- start section all blog -->
    <section>
        <?php


        while (have_posts()){
            the_post();

            ?>
            <div class="card">
                <div class="card-image">
                    <a href="<?php the_permalink(); ?>">
                        <img class="img" src="<?php  echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="Card Image">
                    </a>
                </div>

                <div class="card-description">
                    <a href="<?php  the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                    </a>
                    <div class="card-meta">
                        Posted By <span><?php the_author() ?></span> On <span><?php the_time('F j, Y'); ?> In</span>
                        <?php
                        if (get_post_type() == 'post'){ ?>
                            In <a href=""><?php echo get_the_category_list(', ') ?></a>
                        <?php } ?>
                        ?>
                    </div>
                    <p><?php  echo wp_trim_words(get_the_excerpt() , 15)?></p>
                    <a href="<?php  the_permalink(); ?>" class="btn-readmore">Read more</a>
                </div>
            </div>
            <?php
        }
        wp_reset_query();
        ?>

        <!-- start pagination -->
        <div class="pagination">
            <?php echo paginate_links(); ?>
        </div>
        <!-- end pagination -->

    </section>
    <!-- end section all blog -->
    <?php
         }else{?>
              <div class="not-found">
                 <span style="color: darkgreen;font-weight: bold;font-size: 20px;"> <?php echo 'No result found h\'ere and mistype so don\'t worry try anther so have a nice day';?></span>
              </div>


        <?php }
    ?>





<?php get_footer(); ?>