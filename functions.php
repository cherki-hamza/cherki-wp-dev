<?php

// adding the css and js files
function setup__css_and_js(){
    wp_enqueue_style('google-font' , 'https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed|Roboto+Slab');
    wp_enqueue_style('style' , get_template_directory_uri() .'/assets/css/style.css');
    wp_enqueue_style('fontawesome-css' , get_template_directory_uri() .'/assets/css/all.css');
    wp_enqueue_script('main' , get_template_directory_uri() .'/assets/js/main.js' , array('jquery') , microtime() , true);
    wp_enqueue_script( 'fontawesome-js', get_template_directory_uri() . '/assets/js/all.js' , array('jquery') , microtime() , true);
}

add_action('wp_enqueue_scripts' , 'setup__css_and_js');

// Adding Theme Support
function init_thumbnails(){
    //add the feature image thumbnails to the admin post dashboard
    add_theme_support('post-thumbnails');
    //add the title from general setting
    add_theme_support('title-tag');

    // add support html5
    add_theme_support(
        'html5' ,
        array('comment-list', 'comment-form' , 'search-form')
    );

}
add_action('after_setup_theme' , 'init_thumbnails');

// create Project custom post type
function init_custom_post_type_project(){
    register_post_type(
         'project' ,
         array(
             'rewrite'    => array('slug' => 'projects'),
             'labels'     => array(
                     'name' => 'Projects',
                     'singular_name' => 'Project',
                     'add_new_item'  => 'Add New Project',
                     'edit_item'     => 'Edit Project'
             ),
             'menu_icon'   => 'dashicons-image-filter',
             'public'      => true,
             'has_archive' => true,
             'supports'    => array('title','thumbnail','editor','excerpt','comments','author','custom-fields')
         )
    );
}
add_action('init' , 'init_custom_post_type_project');

// create Portfolio custom post type
function init_custom_post_type_portfolio(){
    register_post_type(
        'portfolio' ,
        array(
            'rewrite'    => array('slug' => 'portfolios'),
            'labels'     => array(
                'name' => 'Portfolio',
                'singular_name' => 'Portfolio',
                'add_new_item'  => 'Add New Portfolio',
                'edit_item'     => 'Edit Portfolio'
            ),
            'menu_icon'   => 'dashicons-groups',
            'public'      => true,
            'has_archive' => true,
            'supports'    => array('title','thumbnail','editor','excerpt','comments','author')
        )
    );
}
add_action('init' , 'init_custom_post_type_portfolio');

// register widget
function get_widget(){
    register_sidebar(array(
       'name' => 'Main Sidebar',
        'id'  => 'main_sidebar',
        'before_title' => '<h3 style="list-style: none;color: #0d99d5;">',
        'after_title' => '</h3>',
    ));
}
add_action('widgets_init' , 'get_widget');

// aff filters to search form
function search_filter($query){
    if ($query->is_search()){
        $query->set('post_type' , array('post','project','portfolio'));
    }
}
add_action('pre_get_posts' , 'search_filter');