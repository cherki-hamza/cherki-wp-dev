<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png"  href="<?php echo get_template_directory_uri(); ?>/img/icon.png" sizes="16x16" />


    <?php wp_head(); ?>
    <title><?php the_title(); ?></title>
</head>

<body>

<!-- start mobile menu -->
<div id="slideout-menu">
    <ul>
        <li>
            <a href="<?php echo site_url('/')?>"
                <?php if (is_front_page()) echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-home"></i>Home</a>
        </li>
        <li>
            <a href="<?php echo site_url('/blog'); ?>"
                <?php if (get_post_type() == 'post') echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-blog"></i>Blog</a>
        </li>
        <li>
            <a href="<?php echo site_url('/projects'); ?>"
                <?php if (get_post_type() == 'project') echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-project-diagram"></i>Projects</a>
        </li>
        <li>
            <a href="<?php echo site_url('/portfolio'); ?>"
                <?php if (get_post_type() == 'portfolio') echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-user-alt-slash"></i>Portfolio</a>
        </li>
        <li>
            <a href="<?php echo site_url('about'); ?>"
                <?php if (is_page('about')) echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-user-cog"></i>About</a>
        </li>
        <li>
            <a href="<?php echo site_url('/contact'); ?>"
                <?php if (is_page('contact')) echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-user"></i>Contact Me</a>
        </li>
        <li>
            <div class="searchbox-slide-menu">
              <?php echo get_search_form()?>
            </div>
        </li>
    </ul>
</div>
<!-- end mobile menu -->

<!-- start Main menu with logo -->
<nav>
    <div id="logo-img">
        <a href="<?php echo site_url('')?>">
            <img src="<?php  echo get_template_directory_uri() ; ?>/img/logo.png" alt="cherki dev">
        </a>
    </div>
    <div id="menu-icon">
        <i class="fas fa-bars"></i>
    </div>
    <ul>
        <li>
            <a href="<?php echo site_url('/'); ?>"
                <?php if (is_front_page()) echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-home"></i>Home</a>
        </li>
        <li>
            <a href="<?php echo site_url('/blog'); ?>"
                <?php if (get_post_type() == 'post') echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-blog"></i>Blog</a>
        </li>
        <li>
            <a href="<?php echo site_url('/projects'); ?>"
                <?php if (get_post_type() == 'project') echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-project-diagram"></i>Projects</a>
        </li>
        <li>
            <a href="<?php echo site_url('/portfolios'); ?>"
                <?php if (get_post_type() == 'portfolio') echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-user-alt-slash"></i>Portfolio</a>
        </li>
        <li>
            <a href="<?php echo site_url('/about'); ?>"
                <?php if (is_page('about')) echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-user-cog"></i>About</a>
        </li>
        <li>
            <a href="<?php echo site_url('/contact'); ?>"
                <?php if (is_page('contact')) echo 'class="active"'?>
            ><i style="margin-right: 3px;" class="fal fa-user"></i>Contact Me</a>
        </li>
        <li>

            <div id="search-icon" class="searchbox-slide-menu">
                <?php echo get_search_form()?>
            </div>
        </li>
    </ul>
</nav>
<!-- end Main menu with logo -->

<div id="search-icon" class="searchbox">
    <?php echo get_search_form()?>
</div>


<?php
    if (!is_front_page()){?>
        <main>

<?php } ?>