<?php get_header();
 while (have_posts()){
     the_post();

?>

<div id="post-container">
<section id="blogpost">
  <h3><?php the_title(); ?></h3>
    <?php the_content(); ?>

</section>
</div>

<?php
}
?>

<?php get_footer(); ?>
