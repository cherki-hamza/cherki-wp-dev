<?php get_header(); ?>


    <!-- start Main content-->
    <main>
    <a href="<?php the_permalink('portfolio'); ?>">
        <h2 class="page-heading">All Portfolios</h2>
    </a>

    <!-- start section all blog -->
    <section>
        <?php


        while (have_posts()){
            the_post();

            ?>
            <div class="card">
                <div class="card-image">
                    <a href="<?php the_permalink(); ?>">
                        <img class="img" src="<?php  echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="Card Image">
                    </a>
                </div>

                <div class="card-description">
                    <a href="<?php  the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                    </a>
                    <div class="card-meta">
                        Posted By <span><?php the_author() ?></span> On <span><?php the_time('F j, Y'); ?></span>
                    </div>
                    <p><?php  echo wp_trim_words(get_the_excerpt() , 15)?></p>
                    <a href="<?php  the_permalink(); ?>" class="btn-readmore">Read more</a>
                </div>
            </div>
            <?php
        }
        wp_reset_query();
        ?>

    </section>
    <!-- end section all blog -->

    <!-- start pagination -->
    <div class="pagination">
        <?php echo paginate_links(); ?>
    </div>
    <!-- end pagination -->


<?php get_footer(); ?>