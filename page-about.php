<?php get_header();

 while (have_posts()){
     the_post();


?>


    <h2 class="page-heading"><?php the_title() ?></h2>
    <div id="post-container">
        <section id="blogpost">
            <div class="card">
                <?php if (has_post_thumbnail()){ ?>
                <div class="card-image">
                    <img src="<?php  echo get_the_post_thumbnail_url(get_the_ID()); ?>" style="width: 500px;height: 400px;" alt="Card Image">
                </div>
                <?php } ?>
                <div class="card-description">
                    <h3>cherki hamza</h3>
                    <p><?php the_content(); ?></p>
                </div>
            </div>

        </section>

<?php
      }
?>

        <aside id="sidebar">
            <h3>Sidebar Heading</h3>
            <p>Sidebar 1</p>
        </aside>
    </div>


<?php get_footer(); ?>