<?php get_header();

while (have_posts()){
the_post();

?>

    <h2 class="page-heading">Single Post</h2>

<div id="post-container">
<section id="blogpost">
    <div class="card">
        <div class="card-image">
            <a href="<?php the_permalink(); ?>">
                <img src="<?php  echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="Card Image">
            </a>
        </div>

        <div class="card-description">
            <a href="blogpost.php">
                <h3><?php the_title(); ?></h3>
            </a>
            <div class="card-meta">
                Posted By <span><?php the_author() ?></span> On <span><?php the_time('F j, Y'); ?> </span>
                <?php
                  if (get_post_type() == 'post'){ ?>
                      In <a href=""><?php echo get_the_category_list(', ') ?></a>
                 <?php } ?>
                ?>

            </div>
              <?php the_content(); ?>


        </div>
    </div>

 <!-- start comments -->
    <div class="comments-section">
        <?php

//        $commenter = array();
//        $aria_req = '';
//        $fields =  array(
//
//            'author' =>
//                    '<input id="author" name="author" type="text" placeholder="Author" value="' . esc_attr(  $commenter['comment_author'] ) .
//                    '" size="30" ' . $aria_req . ' /></p>',
//
//            'email' =>
//                    '<input id="email" name="email" type="text" placeholder="Email" value="' . esc_attr(  $commenter['comment_author_email'] ) .
//                    '" size="30" ' . $aria_req . ' /></p>',
//        );
//        $args = array(
//                'title_replay' => 'Share Your Thoughts',
//                'fields' => $fields,
//                'comment_field' => '<textarea placeholder="your comment" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>',
//                'comment_notes_before' => '<p class="comment-notes">Your email address will not be published. All fields are required.</p>'
//        );

        comment_form();
        $comments_numbers = get_comments_number();
        if ($comments_numbers != 0){ ?>
          <div class="comments">
              <h3>whats others say</h3>
              <ol class="all-comments">
                  <?php
                    $comments = get_comments(array(
                       'post_id' => $post->ID,
                       'status' => 'approve'
                    ));
                    wp_list_comments(array(
                            'per_page' => 15
                       ),
                      $comments
                    )
                  ?>
              </ol>
          </div>
        <?php
        }
        ?>
    </div>
 <!-- end comments -->


<?php
}

?>
</section>

<aside id="sidebar">
    <h4>Sidebar Heading</h4>
      <?php dynamic_sidebar('main_sidebar') ?>
</aside>
</div>

<?php get_footer(); ?>
